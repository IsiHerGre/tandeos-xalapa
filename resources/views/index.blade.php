<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getlocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Tandeos Xalapa</title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp"
/>
		<link href="https://cdn.jsdelivr.net/npm/font-awesome@4.x/css/font-awesome.min.css" rel="stylesheet">
	</head>
	<body>
		<div id="app">
<v-app>

	<index-component> </index-component>
</v-app>
		</div>
		<script src="{{ asset('js/app.js') }}"></script>
	</body>
</html>
