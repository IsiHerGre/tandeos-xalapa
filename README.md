# Tandeos Xalapa

[![](https://xalapacode.com/img/xalapacode/logo.png)](https://xalapacode.com/)

Este proyecto tiene la finalidad de informar a la población de la ciudad de Xalapa de Enriquez Veracruz México, sobre las temporadas de estiaje o tandeos.

La aplicación se compone de lo siguiente:

# Aspectos tecnicos:

  - Laravel 7.x['estable']
  - vue js 2.x['estable']
  - vuetify 2.3.0['estable']


# Instrucciones:
0. Tener previamente instalados los requerimientos de laravel, npm, vue y vuetify.
1. Clonar este repositorio
2. Instalar los complementos de laravel
2.1 $ composer install
3. instalar vue y vuetify en el proyecto
3.1 $ npm install
4. configurar el .env para utilizar algún SGBD
5. Realizar las migraciones con composer o utilizar el respaldo en blanco que se encuentra en la raiz del proyecto (back.sql)
5.1 $ php artisan migrate
6. Crear un usuario para el administrador
6.1 descomentar la ruta 'api/register' en el directorio 'Routes/api.php'
6.1.1 realizar una peticion post a la ruta 'api/post' con los parametros ['name', 'email', 'password', 'c_password']
6.2 Tambien se puede crear el usuario desde la consola de laravel tinker
7. configurar el proyecto con algun servidor
7.1 $ php artisan serve
8 compilar los archivos vue a desarrollo o a produccion
8.1 $ npm run watch o $ npm run build  



License
----

MIT

Contacto del administrador: isihergre@gmail.com

