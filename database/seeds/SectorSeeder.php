<?php

use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  { 
    factory(App\Sector::class, 10)->create()->each(function($sector){
      factory(App\Zone::class, 10)->make()->each(function($zone) use ($sector){
        $sector->zones()->save($zone); 
        factory(App\Colony::class, 10)->make()->each(function($colony) use ($zone){
          $zone->colonies()->save($colony); 
        });
      });
    });

  }
}
