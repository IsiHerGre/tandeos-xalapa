<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $fillable = [
        'zone_id', 'date_start', 'date_end',
  ];

    public $timestamps = false;
}
