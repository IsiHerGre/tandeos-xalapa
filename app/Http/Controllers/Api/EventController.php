<?php

namespace App\Http\Controllers\Api;

use App\Event;
use App\Zone;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();

        return response()->json($events);
    }

	public function getEventsFromZone(Request $request)
	{
		$events = Event::Select('date_start as start', 'date_end as end', 'type as name')->where('zone_id', '=', $request->zone_id)->get();

		foreach ($events as $event) {
			$event->color = 'blue';
		}

		return response()->json($events);
	}

	public function getEventsFromSector(Request $request)
	{
		$sector_id = $request->sector_id;
		$date = date("Y-m-d H:i:s", strtotime($request->date));

		$events = DB::table('zones')
            ->rightJoin('events', 'zones.id', '=', 'events.zone_id')
			->where('zones.sector_id', '=', $sector_id)
			->whereDate('events.date_start', '>=', $date)
			->Select('events.date_start as start', 'events.date_end as end', 'events.type as name')
			->get();

		foreach ($events as $event) {
			$event->color = 'blue';
		}
		return response()->json($events);


	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$event = new Event;
		$event->date_start = $request->date_start;
		$event->date_end = $request->date_end;
		$event->type = 'tandeo';
		$event->zone_id = $request->zone_id;

		$event->saveOrFail();

		return response()->json($event);

	}

	public function storeEvents(Request $request)
	{
		$zone_id = $request->zone_id;
		$dates = $request->dates;
		foreach ($dates as $date) {
			/*Event::Create([
				'date_start' => $date,
				'date_end' => $date,
				'type' => 'tandeo',
				'zone_id' => $zone_id
			]);*/
			$event = new Event;
			$event->date_start = $date;
			$event->date_end = $date;
			$event->type = 'tandeo';
			$event->zone_id = $zone_id;

			$event->saveOrFail();

		}
		return response()->json([
			'status'=> 'success',
			'message' => 'Elementos Creados'
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Event  $event
	 * @return \Illuminate\Http\Response
	 */
	public function show(Event $event)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Event  $event
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Event $event)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Event  $event
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Event $event)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Event  $event
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Event $event)
	{
		//
	}
}
