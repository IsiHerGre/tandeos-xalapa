<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
	//
	
	protected $fillable = [
		'name', 'sector_id', 
	];


	public function events()
	{
		return $this->hasMany(Event::class);
	}


  public function colonies(){
    return $this->hasMany("App\Colony");
  }

}
